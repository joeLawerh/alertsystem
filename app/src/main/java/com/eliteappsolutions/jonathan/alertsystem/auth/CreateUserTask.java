package com.eliteappsolutions.jonathan.alertsystem.auth;

import android.content.Context;
import android.os.AsyncTask;
import android.text.format.Time;
import android.util.Log;

import com.eliteappsolutions.jonathan.alertsystem.backend.userApi.UserApi;
import com.eliteappsolutions.jonathan.alertsystem.backend.userApi.model.User;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.extensions.android.json.AndroidJsonFactory;

/**
 * Created by jonathan on 1/29/15.
 */

public class CreateUserTask extends AsyncTask<Object, Void, User> {

    private final String LOG_TAG = "CreateUserTask";

    private Context context;
    private AuthUser authUser = null;

    public CreateUserTask(Context context) {
        this.context = context;
        this.authUser = AuthUser.getInstance(context);
    }

    private void updateUserInfoOnDevice(User user) {
        Time time = new Time();
        time.setToNow();
        authUser.setRegistrationDate(Long.toString(time.toMillis(false)));
        authUser.setIsRegistered(true);
        authUser.setSignedIn(true);
        authUser.setUserId(user.getId());
        authUser.setEmail(user.getEmail());
        authUser.setFullName(user.getName());
        authUser.setPhoneNo(user.getPhoneNo());
        authUser.setGender(user.getGender());
        authUser.setLocation(user.getLocation());
        authUser.setDonationDate(user.getDonationDate());
        authUser.setBloodGroup(user.getBloodGroup());
        authUser.setIsDonor(user.getDonor());
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected User doInBackground(Object... params) {

        User user = null;

        try {
            GoogleApiClient googleApiClient = (GoogleApiClient) params[0];
            Person person = Plus.PeopleApi.getCurrentPerson(googleApiClient);
            String email  = Plus.AccountApi.getAccountName(googleApiClient);

            if (person == null) {
                throw new Exception("Person information is null");
            }

            UserApi.Builder userApi = new UserApi.Builder(AndroidHttp.newCompatibleTransport(),
                    new AndroidJsonFactory(), null)
                    .setRootUrl("https://apps-233-moja.appspot.com/_ah/api/");
            UserApi service = userApi.build();

            user = service.getByEmail(email).execute();

            if(user != null) {


                return user;
            }

            user = new User();
            user.setEmail(Plus.AccountApi.getAccountName(googleApiClient));
            user.setName(person.getDisplayName());
            user.setPhoneNo("");
            user.setGender(Integer.toString(person.getGender()));
            user.setLocation(person.getCurrentLocation());
            user.setDonationDate("2014-08-01");
            user.setBloodGroup("Unknown");
            user.setDonor(false);

            user = service.insert(user).execute();
        } catch (Exception e) {
            Log.e(LOG_TAG, e.getMessage());
            e.printStackTrace();
        }

        return user;
    }

    @Override
    protected void onPostExecute(User user) {
        super.onPostExecute(user);
        if (user == null || user.getId() == null) {
            Log.e(LOG_TAG, "Sorry we could not create your account. Please try again");
        } else {
            updateUserInfoOnDevice(user);
            Log.i(LOG_TAG, "Your account was created successfully");
        }
    }
}
