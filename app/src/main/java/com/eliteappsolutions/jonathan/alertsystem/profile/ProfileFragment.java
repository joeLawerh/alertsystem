package com.eliteappsolutions.jonathan.alertsystem.profile;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.RingtonePreference;
import android.text.TextUtils;

import com.github.stephenbaidu.placepicker.PlaceDetail;
import com.eliteappsolutions.jonathan.alertsystem.R;
import com.eliteappsolutions.jonathan.alertsystem.auth.AuthUser;
import com.eliteappsolutions.jonathan.alertsystem.util.Config;
import com.github.stephenbaidu.placepicker.PlacePicker;


/**
 * Created by stephen on 1/28/15.
 */
public class ProfileFragment extends PreferenceFragment implements Preference.OnPreferenceClickListener {

    public ProfileFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getPreferenceManager().setSharedPreferencesName(AuthUser.PREF_FILE_NAME);
        addPreferencesFromResource(R.xml.pref_profile);

        bindPreferenceSummaryToValue(findPreference(AuthUser.KEY_EMAIL));
        bindPreferenceSummaryToValue(findPreference(AuthUser.KEY_FULL_NAME));
        bindPreferenceSummaryToValue(findPreference(AuthUser.KEY_GENDER));
        bindPreferenceSummaryToValue(findPreference(AuthUser.KEY_LOCATION));
        bindPreferenceSummaryToValue(findPreference(AuthUser.KEY_PHONE_NO));

        findPreference(AuthUser.KEY_LOCATION).setOnPreferenceClickListener(this);
    }

    /**
     * A preference value change listener that updates the preference's summary
     * to reflect its new value.
     */
    private static Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener = new Preference.OnPreferenceChangeListener() {
        @Override
        public boolean onPreferenceChange(Preference preference, Object value) {
            String stringValue = value.toString();

            if (preference instanceof ListPreference) {
                // For list preferences, look up the correct display value in
                // the preference's 'entries' list.
                ListPreference listPreference = (ListPreference) preference;
                int index = listPreference.findIndexOfValue(stringValue);

                // Set the summary to reflect the new value.
                preference.setSummary(
                        index >= 0
                                ? listPreference.getEntries()[index]
                                : null);

            } else if (preference instanceof RingtonePreference) {
                // For ringtone preferences, look up the correct display value
                // using RingtoneManager.
                if (TextUtils.isEmpty(stringValue)) {
                    // Empty values correspond to 'silent' (no ringtone).
//                    preference.setSummary(R.string.pref_ringtone_silent);

                } else {
                    Ringtone ringtone = RingtoneManager.getRingtone(
                            preference.getContext(), Uri.parse(stringValue));

                    if (ringtone == null) {
                        // Clear the summary if there was a lookup error.
                        preference.setSummary(null);
                    } else {
                        // Set the summary to reflect the new ringtone display
                        // name.
                        String name = ringtone.getTitle(preference.getContext());
                        preference.setSummary(name);
                    }
                }

            } else {
                // For all other preferences, set the summary to the value's
                // simple string representation.
                preference.setSummary(stringValue);
            }
            return true;
        }
    };

    /**
     * Binds a preference's summary to its value. More specifically, when the
     * preference's value is changed, its summary (line of text below the
     * preference title) is updated to reflect the value. The summary is also
     * immediately updated upon calling this method. The exact display format is
     * dependent on the type of preference.
     *
     * @see #sBindPreferenceSummaryToValueListener
     */
    private static void bindPreferenceSummaryToValue(Preference preference) {
        // Set the listener to watch for value changes.
        preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);

        // Trigger the listener immediately with the preference's
        // current value.
        sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                preference.getContext().getSharedPreferences(AuthUser.PREF_FILE_NAME, Context.MODE_PRIVATE)
                        .getString(preference.getKey(), ""));
    }

    @Override
    public boolean onPreferenceClick(Preference preference) {
        if (preference.getKey().equals(AuthUser.KEY_LOCATION)) {
            Intent intent = new Intent(getActivity().getApplicationContext(), PlacePicker.class);
            intent.putExtra(PlacePicker.PARAM_API_KEY, Config.API_KEY);
            intent.putExtra(PlacePicker.PARAM_EXTRA_QUERY, "&components=country:gh&types=(cities)");

            startActivityForResult(intent, PlacePicker.REQUEST_CODE_PLACE);

            return true;
        }
        return false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == PlacePicker.REQUEST_CODE_PLACE && resultCode == Activity.RESULT_OK) {
            PlaceDetail placeDetail = PlacePicker.fromIntent(data);
            findPreference(AuthUser.KEY_LOCATION).setSummary(placeDetail.description);
            AuthUser.getInstance(getActivity()).setLocation(placeDetail.description);
        }
    }

}
