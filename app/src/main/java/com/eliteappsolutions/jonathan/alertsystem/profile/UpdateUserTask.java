package com.eliteappsolutions.jonathan.alertsystem.profile;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.eliteappsolutions.jonathan.alertsystem.backend.userApi.UserApi;
import com.eliteappsolutions.jonathan.alertsystem.backend.userApi.model.User;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.extensions.android.json.AndroidJsonFactory;
import com.eliteappsolutions.jonathan.alertsystem.auth.AuthUser;

import java.io.IOException;

/**
 * Created by stephen on 1/29/15.
 */
public class UpdateUserTask extends AsyncTask<String, Void, User> {

    private final String LOG_TAG = "UpdateUserTask";

    private Context context;
    private AuthUser authUser = null;

    public UpdateUserTask(Context context) {
        this.context = context;
        this.authUser = AuthUser.getInstance(context);
    }

    private void updateUserInfoOnDevice(User user) {
        authUser.setIsRegistered(true);
        authUser.setSignedIn(true);
        authUser.setUserId(user.getId());
        authUser.setEmail(user.getEmail());
        authUser.setFullName(user.getName());
        authUser.setPhoneNo(user.getPhoneNo());
        authUser.setGender(user.getGender());
        authUser.setLocation(user.getLocation());
    }

    @Override
    protected User doInBackground(String... params) {

        User user = null;

        try {
            UserApi.Builder userApi = new UserApi.Builder(AndroidHttp.newCompatibleTransport(),
                    new AndroidJsonFactory(), null)
                    .setRootUrl("https://apps-233-moja.appspot.com/_ah/api/");
            UserApi service = userApi.build();

            user = service.getByEmail(params[0]).execute();

            // Email is excluded from the update
            user.setName(authUser.getFullName());
            user.setPhoneNo(authUser.getPhoneNo());
            user.setGender(authUser.getGender());
            user.setLocation(authUser.getLocation());

            try {
                user = service.update(user.getId(), user).execute();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            Log.e(LOG_TAG, e.getMessage());
            e.printStackTrace();
        }

        return user;
    }

    @Override
    protected void onPostExecute(User user) {
        super.onPostExecute(user);
        if (user == null || user.getId() == null) {
            Log.e(LOG_TAG, "Failed to update user profile");
        } else {

            updateUserInfoOnDevice(user);
            Log.i(LOG_TAG, "User profile updated successfully");
        }
    }
}