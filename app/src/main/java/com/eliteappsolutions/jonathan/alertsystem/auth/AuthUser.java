package com.eliteappsolutions.jonathan.alertsystem.auth;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by stephen on 1/27/15.
 */
public class AuthUser {

    private static AuthUser instance = null;

    private SharedPreferences prefs = null;
    public static final String PREF_FILE_NAME = "AuthUser";

    // Key fields
    public static final String KEY_IS_REGISTERED = "user_is_registered";
    public static final String KEY_SIGNED_IN = "user_signed_in";
    public static final String KEY_USER_ID = "user_user_id";
    public static final String KEY_EMAIL = "user_email";
    public static final String KEY_FULL_NAME = "user_name";
    public static final String KEY_PHONE_NO = "user_phone_no";
    public static final String KEY_GENDER = "user_gender";
    public static final String KEY_LOCATION = "user_location";
    public static final String KEY_DONATION_DATE = "user_donation_datee";
    public static final String KEY_BLOOD_GROUP = "user_blood_group";
    public static final String KEY_LATITUDE = "user_latitude";
    public static final String KEY_LONGITUDE = "user_longitude";
    public static final String KEY_IS_DONOR = "user_is_donor";
    public static final String KEY_REGISTRATION_DATE = "user_registered_date";

    protected AuthUser(Context context) {
        this.prefs = context.getSharedPreferences(AuthUser.PREF_FILE_NAME, Context.MODE_PRIVATE);
    }

    public static AuthUser getInstance(Context context) {
        if (instance == null) {
            instance = new AuthUser(context);
        }

        return instance;
    }

    public boolean isRegistered() {
        return prefs.getBoolean(AuthUser.KEY_IS_REGISTERED, false);
    }

    public void setIsRegistered(boolean isRegistered) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(AuthUser.KEY_IS_REGISTERED, isRegistered);
        editor.apply();
    }

    public boolean signedIn() {
        return prefs.getBoolean(AuthUser.KEY_SIGNED_IN, false);
    }

    public void setSignedIn(boolean signedIn) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(AuthUser.KEY_SIGNED_IN, signedIn);
        editor.apply();
    }

    public String registrationDate() {
        return prefs.getString(AuthUser.KEY_REGISTRATION_DATE, "");
    }

    public void setRegistrationDate(String registrationDate) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(AuthUser.KEY_REGISTRATION_DATE, registrationDate);
        editor.apply();
    }

    public Long getUserId() {
        return prefs.getLong(AuthUser.KEY_USER_ID, 0);
    }

    public void setUserId(Long userId) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putLong(AuthUser.KEY_USER_ID, userId);
        editor.apply();
    }

    public String getEmail() {
        return prefs.getString(AuthUser.KEY_EMAIL, "");
    }

    public void setEmail(String email) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(AuthUser.KEY_EMAIL, email);
        editor.apply();
    }

    public String getFullName() {
        return prefs.getString(AuthUser.KEY_FULL_NAME, "");
    }

    public void setFullName(String fullName) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(AuthUser.KEY_FULL_NAME, fullName);
        editor.apply();
    }

    public String getPhoneNo() {
        return prefs.getString(AuthUser.KEY_PHONE_NO, "");
    }

    public void setPhoneNo(String phoneNo) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(AuthUser.KEY_PHONE_NO, phoneNo);
        editor.apply();
    }

    public String getGender() {
        return prefs.getString(AuthUser.KEY_GENDER, "");
    }

    public void setGender(String gender) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(AuthUser.KEY_GENDER, gender);
        editor.apply();
    }

    public String getLocation() {
        return prefs.getString(AuthUser.KEY_LOCATION, "");
    }

    public void setLocation(String location) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(AuthUser.KEY_LOCATION, location);
        editor.apply();
    }

    public String getDonationDate() {return prefs.getString(AuthUser.KEY_DONATION_DATE, "");}

    public void setDonationDate(String donationDate) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(AuthUser.KEY_DONATION_DATE, donationDate);
        editor.apply();
    }

    public String getBloodGroup() {
        return prefs.getString(AuthUser.KEY_BLOOD_GROUP, "");
    }

    public void setBloodGroup(String bloodGroup) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(AuthUser.KEY_BLOOD_GROUP, bloodGroup);
        editor.apply();
    }

    public String getLatitude() {
        return prefs.getString(AuthUser.KEY_LATITUDE, "");
    }

    public void setLatitude(String latitude) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(AuthUser.KEY_LATITUDE, latitude);
        editor.apply();
    }

    public String getLongitude() {
        return prefs.getString(AuthUser.KEY_LONGITUDE, "");
    }

    public void setLongitude(String longitude) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(AuthUser.KEY_LONGITUDE, longitude);
        editor.apply();
    }

    public boolean getIsDonor() {
        return prefs.getBoolean(AuthUser.KEY_IS_DONOR, false);
    }

    public void setIsDonor(boolean isDonor) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(AuthUser.KEY_IS_DONOR, isDonor);
        editor.apply();
    }


    @Override
    public String toString() {
        return String.format("Email: %s,Name: %s,Gender: %s,Location: %s,donationDate: %s,PhoneNo: %s,Blood: %s",
                getEmail(), getFullName(), getGender(), getLocation(), getDonationDate(),
                getPhoneNo(), getBloodGroup());
    }
}
