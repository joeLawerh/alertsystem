package com.eliteappsolutions.jonathan.alertsystem.safety_notifications;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.ShareActionProvider;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.eliteappsolutions.jonathan.alertsystem.R;
import com.eliteappsolutions.jonathan.alertsystem.backend.registration.Registration;
import com.eliteappsolutions.jonathan.alertsystem.dashboard.DashboardActivity;
import com.eliteappsolutions.jonathan.alertsystem.util.Config;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.regex.Pattern;

public class AlertNotificationActivity extends ActionBarActivity  {
	TextView usertitleET, msgET;
	private EditText textEntry;
	private Button shareButton;
	private Button dashboardButton;
    private ShareActionProvider mShareActionProvider;
    Intent mShareIntent;
    private Button fabButtonBack;
    private Button fabdashboard;
    private Registration service;
    String currentMessage;
    GoogleCloudMessaging gcm;
    ImageView imageView;
    private LinearLayout linearLayout, messageLinearLayout;


    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    public static final String URL =
            "http://www.eliteappsolutions.com/wp-content/uploads/2016/04/healthtips.jpg";


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_health_notifications);
        imageView = (ImageView) findViewById(R.id.imageView);
        msgET = (TextView) findViewById(R.id.message);

        String userEntry = msgET.getText().toString();

        linearLayout = (LinearLayout) findViewById(R.id.eventTypeThemeLayout);

        messageLinearLayout = (LinearLayout) findViewById(R.id.messagelinear);

		//String txtMessage = msgET.getText().toString().trim();
		//String[] params = {txtMessage};

		shareButton = (Button)findViewById(R.id.share_text_button);
		//dashboardButton = (Button)findViewById(R.id.share_admin);

        /**fab = (FloatingActionButton)findViewById(R.id.fabButton);
		fab.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
		fab.setDrawableIcon(getResources().getDrawable(R.drawable.back));**/

        fabdashboard = (Button)findViewById(R.id.fabButton);

        fabButtonBack = (Button)findViewById(R.id.fabButtonBack);
		//setupEvents();
        setupActionBar();
		setUpButton();

		// Intent Message sent from Broadcast Receiver
		String str = getIntent().getStringExtra("msg");

		if (!checkPlayServices()) {
			Toast.makeText(
					getApplicationContext(),
					"This device doesn't support Play services, App will not work normally",
					Toast.LENGTH_LONG).show();
		}

		//usertitleET.setText("Hello " + " !");
		// When Message sent from Broadcast Receiver is not empty
		if (str != null) {
			//msgET = (TextView) findViewById(R.id.message);
            if (checkURL(str) == true) {
                // Create an object for subclass of AsyncTask
                GetXMLTask task = new GetXMLTask();
                // Execute the task
                task.execute(new String[]{str});
                imageView.setVisibility(View.VISIBLE);

            }
            else{
                msgET.setText(str);
                msgET.setVisibility(View.VISIBLE);
                linearLayout.setVisibility(View.VISIBLE);
                messageLinearLayout.setVisibility(View.VISIBLE);
            }

		}


	}

    public static boolean checkURL(CharSequence input) {
        if (TextUtils.isEmpty(input)) {
            return false;
        }
        Pattern URL_PATTERN = Patterns.WEB_URL;
        boolean isURL = URL_PATTERN.matcher(input).matches();
        if (!isURL) {
            String urlString = input + "";
            if (URLUtil.isNetworkUrl(urlString)) {
                try {
                    new URL(urlString);
                    isURL = true;
                } catch (Exception e) {
                }
            }
        }
        return isURL;
    }

    @Override
    protected void onStart() {
        super.onStart();
        SharedPreferences preferences = getPreferences(Context.MODE_PRIVATE);

        String userEntry = preferences.getString(Config.USER_TIP, "");

        msgET.setText(userEntry);


        Log.d("Message is retrieved", userEntry);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate menu resource file.
        getMenuInflater().inflate(R.menu.share_menu, menu);

		// Find the MenuItem that we know has the ShareActionProvider
		//MenuItem item = menu.findItem(R.id.menu_item_share);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_share:

                shareIt();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

	public void setUpButton(){

		/**fab.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AlertNotificationActivity.this, DashboardActivity.class);
                startActivity(intent);
            }
        });**/


        fabdashboard.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = msgET.getText().toString();
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Alert Tips");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Share Tips via"));

                //startActivity(textShareIntent);
                // ^^ this auto-picks the defined default program for a content type, but since we want users to
                //    have options, we instead use the OS to create a chooser for users to pick from

                startActivity(Intent.createChooser(sharingIntent, "Share Tips with..."));
            }
        });

        fabButtonBack.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                startActivity(new Intent(AlertNotificationActivity.this, DashboardActivity.class));
            }
        });
    }


	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	public void setupActionBar() {
		/**if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
		 android.app.ActionBar actionBar = getActionBar();
		 actionBar.setDisplayHomeAsUpEnabled(true);
		 }**/
	}

    private void shareIt() {
//sharing implementation here
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareBody = msgET.getText().toString();
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Alert Tips");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent, "Share Tips via"));
    }

	private boolean checkPlayServices() {
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(this);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				GooglePlayServicesUtil.getErrorDialog(resultCode, this,
						PLAY_SERVICES_RESOLUTION_REQUEST).show();
			}
			else {
				Toast.makeText(
						getApplicationContext(),
						"This device doesn't support Play services, App will not work normally",
						Toast.LENGTH_LONG).show();
				finish();
			}
			return false;
		}
        /**else {
			Toast.makeText(
					getApplicationContext(),
					"This device supports Play services, App will work normally",
					Toast.LENGTH_LONG).show();
		}**/
		return true;
	}
    @Override
    protected void onStop() {
        super.onStop();
        String userEntry = msgET.getText().toString();

        SharedPreferences preferences = getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putString(Config.USER_TIP, userEntry);


        editor.commit();
    }

	@Override
	protected void onResume() {
		super.onResume();
		checkPlayServices();
        String str = getIntent().getStringExtra("msg");
        msgET.setText(str);
        msgET.setVisibility(View.VISIBLE);
        linearLayout.setVisibility(View.VISIBLE);
        messageLinearLayout.setVisibility(View.VISIBLE);

	}

    private class GetXMLTask extends AsyncTask<String, Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... urls) {
            Bitmap map = null;
            for (String url : urls) {
                map = downloadImage(url);
            }
            return map;
        }

        // Sets the Bitmap returned by doInBackground
        @Override
        protected void onPostExecute(Bitmap result) {
            imageView.setImageBitmap(result);
        }

        // Creates Bitmap from InputStream and returns it
        private Bitmap downloadImage(String url) {
            Bitmap bitmap = null;
            InputStream stream = null;
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            bmOptions.inSampleSize = 1;

            try {
                stream = getHttpConnection(url);
                bitmap = BitmapFactory.
                        decodeStream(stream, null, bmOptions);
                stream.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return bitmap;
        }

        // Makes HttpURLConnection and returns InputStream
        private InputStream getHttpConnection(String urlString)
                throws IOException {
            InputStream stream = null;
            URL url = new URL(urlString);
            URLConnection connection = url.openConnection();

            try {
                HttpURLConnection httpConnection = (HttpURLConnection) connection;
                httpConnection.setRequestMethod("GET");
                httpConnection.connect();

                if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    stream = httpConnection.getInputStream();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return stream;
        }
    }
}
