package com.eliteappsolutions.jonathan.alertsystem.notifications;

public interface ApplicationConstants {

	// Php Application URL to store Reg ID created
	static final String APP_SERVER_URL = "https://apps-233-moja.appspot.com//_ah/api/";

	// Google Project Number
	static final String GOOGLE_PROJ_ID = "141759949166";
	// Message Key
	static final String MSG_KEY = "m";

}
