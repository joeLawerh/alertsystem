package com.eliteappsolutions.jonathan.alertsystem.safety_notifications;

import android.os.Parcel;
import android.os.Parcelable;

import com.eliteappsolutions.jonathan.alertsystem.backend.registration.model.RegistrationRecord;

/**
 * Created by jonathan on 11/12/2015.
 */
public class StoredMessage {
    public static final String MESSAGE_ID = "MESSAGE_ID";
    public static final String MESSAGE_CLASS = "MESSAGE";
    public static final String NOTIFICATION_MESSAGE = "notification-message";
    Long id;
    private String regId;
    private String message;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRegId() {
        return regId;
    }

    public void setRegId(String regId) {
        this.regId = regId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private StoredMessage() {

    }

    public static StoredMessage fromRegistrationRecord(RegistrationRecord registrationRecord) {
        StoredMessage storedMessage = new StoredMessage();
        storedMessage.setMessage(registrationRecord.getMessage());
        storedMessage.setRegId(registrationRecord.getRegId());


        return storedMessage;
    }

    protected StoredMessage(Parcel in) {
        id = in.readByte() == 0x00 ? null : in.readLong();
        message = in.readString();
        regId = in.readString();
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<StoredMessage> CREATOR = new Parcelable.Creator<StoredMessage>() {
        @Override
        public StoredMessage createFromParcel(Parcel in) {
            return new StoredMessage(in);
        }

        @Override
        public StoredMessage[] newArray(int size) {
            return new StoredMessage[size];
        }
    };
}
