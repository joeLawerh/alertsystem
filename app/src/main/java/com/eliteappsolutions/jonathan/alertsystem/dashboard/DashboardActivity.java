package com.eliteappsolutions.jonathan.alertsystem.dashboard;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.CardView;
import android.telephony.SmsManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.eliteappsolutions.jonathan.alertsystem.MapsActivity;
import com.eliteappsolutions.jonathan.alertsystem.R;
import com.eliteappsolutions.jonathan.alertsystem.auth.AuthUser;
import com.eliteappsolutions.jonathan.alertsystem.profile.ProfileActivity;
import com.eliteappsolutions.jonathan.alertsystem.safety_notifications.AlertNotificationActivity;

public class DashboardActivity extends ActionBarActivity {
    private final String EXTRA_PROFILE_UPDATE_REQUIRED = "profile_update_required";

    //    private RecyclerView mRecyclerView;
//    private CountryAdapter mAdapter;
    SharedPreferences sharedp;

    //private ConnectionConfiguration connConfig;

    AuthUser authUser = null;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
        getSupportActionBar().hide();
        //auth user
        authUser = AuthUser.getInstance(this);



    }

    @Override
    protected void onStart() {
        super.onStart();
        boolean profileUpdateRequired = getIntent().getBooleanExtra(EXTRA_PROFILE_UPDATE_REQUIRED, false);
        String email = getIntent().getStringExtra("email");

        // Redirect to ProfileActivity
        if (profileUpdateRequired) {
            getIntent().putExtra(EXTRA_PROFILE_UPDATE_REQUIRED, false);
            Intent intent = new Intent(DashboardActivity.this, ProfileActivity.class);
            if (email != null) {
                intent.putExtra("email", email);
            }
            startActivity(intent);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        AuthUser authUser = null;
        public PlaceholderFragment() {
        }

        private void circularizeImageViews(View view) {
            int[] imageViews = {R.id.image_view1, R.id.image_view2, R.id.image_view3, R.id.image_view4};

            for (int i = 0; i < imageViews.length; i++) {
                final ImageView img = (ImageView) view.findViewById(imageViews[i]);
                ViewTreeObserver vto = img.getViewTreeObserver();
                vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                    public boolean onPreDraw() {
                        int x;
                        img.getViewTreeObserver().removeOnPreDrawListener(this);
                        x = img.getMeasuredHeight();
                        img.setLayoutParams(new LinearLayout.LayoutParams(x, x));
                        return true;
                    }
                });
            }
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_dashboard, container, false);

            CardView cardView0 = (CardView) rootView.findViewById(R.id.card_view0);
            CardView cardView1 = (CardView) rootView.findViewById(R.id.card_view1);
            CardView cardView2 = (CardView) rootView.findViewById(R.id.card_view2);
            CardView cardView3 = (CardView) rootView.findViewById(R.id.card_view3);
            CardView cardView4 = (CardView) rootView.findViewById(R.id.card_view4);

            circularizeImageViews(rootView);

            authUser = AuthUser.getInstance(getContext());

            cardView1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String messageToSend = "Robbery Alert Has Been received by Police";
                    String number = authUser.getPhoneNo();

                    SmsManager.getDefault().sendTextMessage(number, null, messageToSend, null,null);
                    Toast.makeText(getActivity(), "Kindly wait for response", Toast.LENGTH_LONG).show();
                }
            });

            cardView2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String messageToSend = "Fire Alert has been received by Fire Service";
                    String number = authUser.getPhoneNo();

                    SmsManager.getDefault().sendTextMessage(number, null, messageToSend, null,null);
                    Toast.makeText(getActivity(), "Kindly wait for response", Toast.LENGTH_LONG).show();
                }
            });

            cardView3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getActivity(), ProfileActivity.class));
                }
            });

            cardView4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getActivity(), MapsActivity.class));
                }
            });

            /**cardView5.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getActivity(), ComingSoonActivity.class));
                }
            });

            cardView6.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getActivity(), ComingSoonActivity.class));

                }
            });**/
            return rootView;
        }
    }
}
