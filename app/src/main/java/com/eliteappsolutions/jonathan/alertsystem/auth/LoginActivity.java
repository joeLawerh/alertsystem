package com.eliteappsolutions.jonathan.alertsystem.auth;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.eliteappsolutions.jonathan.alertsystem.dashboard.DashboardActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.eliteappsolutions.jonathan.alertsystem.R;
import com.eliteappsolutions.jonathan.alertsystem.MapsActivity;

public class LoginActivity extends Activity implements ConnectionCallbacks, OnConnectionFailedListener {

    ProgressDialog prgDialog;
    GoogleCloudMessaging gcmObj;
    Context applicationContext;
    String regId = "";

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    AsyncTask<Void, Void, String> createRegIdTask;

    public static final String REG_ID = "regId";
    public static final String EMAIL_ID = "eMailId";
    EditText emailET;

    private AuthUser authUser = null;

    private static final int REQUEST_CODE_SIGN_IN = 0;

    private static int SPLASH_TIME_OUT = 1000;

    private static final String LOG_TAG = "LoginActivity";

    private GoogleApiClient googleApiClient;

    /**
     * A flag indicating that a PendingIntent is in progress and prevents us
     * from starting further intents.
     */
    private boolean mIntentInProgress;

    private boolean mSignInClicked;

    private ConnectionResult mConnectionResult;

    private SignInButton signInButton;
    private TextView alertMsgTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        authUser = AuthUser.getInstance(this);
        setContentView(R.layout.activity_login);

        new GcmRegistrationAsyncTask(this).execute();

       Toast.makeText(this, "Please Sign In", Toast.LENGTH_LONG);


        signInButton = (SignInButton) findViewById(R.id.sign_in_button);
        alertMsgTextView = (TextView) findViewById(R.id.alert_msg_text_view);

        signInButton.setVisibility(View.INVISIBLE);
        alertMsgTextView.setVisibility(View.INVISIBLE);

        if (signInButton != null) {
            // Set on click listener
            signInButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertMsgTextView.setText("Connecting...");
                    alertMsgTextView.setVisibility(View.VISIBLE);
                    signInButton.setVisibility(View.INVISIBLE);
                    signInWithGplus();
                }
            });
            signInButton.setColorScheme(SignInButton.COLOR_LIGHT);

            // Set text to "Sign in with Google"
            for (int i = 0; i < signInButton.getChildCount(); i++) {
                View v = signInButton.getChildAt(i);

                if (v instanceof TextView) {
                    TextView tv = (TextView) v;
                    tv.setText(com.google.android.gms.R.string.common_signin_button_text_long);
                    return;
                }
            }
        }

        if (googleApiClient == null) {
            initializeGoogleApiClient();
        }
    }

    private void initializeGoogleApiClient() {
        // Initializing google plus api client
        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API)
                .addScope(Plus.SCOPE_PLUS_LOGIN).build();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (googleApiClient == null) {
            initializeGoogleApiClient();
        }
        googleApiClient.connect();
    }

    @Override
    public void onStop() {
        googleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        if (!result.hasResolution()) {
            GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), this,
                    0).show();
            return;
        }

        if (!mIntentInProgress) {
            // Store the ConnectionResult for later usage
            mConnectionResult = result;

            if (mSignInClicked) {
                // The user has already clicked 'sign-in' so we attempt to
                // resolve all
                // errors until the user is signed in, or they cancel.
                resolveSignInError();
            } else {
                alertMsgTextView.setVisibility(View.INVISIBLE);
                signInButton.setVisibility(View.VISIBLE);
            }
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int responseCode, Intent intent) {
        if (requestCode == REQUEST_CODE_SIGN_IN) {
            if (responseCode != RESULT_OK) {
                mSignInClicked = false;
            }

            mIntentInProgress = false;

            if (!googleApiClient.isConnecting()) {
                googleApiClient.connect();
            }
        }
    }

    @Override
    public void onConnected(Bundle arg0) {
        mSignInClicked = false;

        signInButton.setVisibility(View.INVISIBLE);

        if (authUser.isRegistered()) {
            Intent intent = new Intent(LoginActivity.this, DashboardActivity.class);
            startActivity(intent);
            finish();
        } else {
            // Register user if first time
            registerUser();
        }
    }

    @Override
    public void onConnectionSuspended(int arg0) {
        googleApiClient.connect();
        signInButton.setVisibility(View.VISIBLE);
    }

    /**
     * Sign-in into google
     * */
    private void signInWithGplus() {
        if (!googleApiClient.isConnecting()) {
            mSignInClicked = true;
            resolveSignInError();
        }
    }

    /**
     * Method to resolve any signin errors
     * */
    private void resolveSignInError() {
        if (mConnectionResult.hasResolution()) {
            try {
                mIntentInProgress = true;
                mConnectionResult.startResolutionForResult(this, REQUEST_CODE_SIGN_IN);
            } catch (IntentSender.SendIntentException e) {
                mIntentInProgress = false;
                googleApiClient.connect();
            }
        }
    }

    /**
     * Sign-out from google
     * */
    private void signOutFromGplus() {
        if (googleApiClient.isConnected()) {
            googleApiClient.clearDefaultAccountAndReconnect();
            googleApiClient.disconnect();
            googleApiClient.connect();
        }
    }

    /**
     * Revoking access from google
     * */
    private void revokeGplusAccess() {
        if (googleApiClient.isConnected()) {
            googleApiClient.clearDefaultAccountAndReconnect();
            Plus.AccountApi.revokeAccessAndDisconnect(googleApiClient)
                    .setResultCallback(new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status arg0) {
                            Log.e(LOG_TAG, "User access revoked!");
                            googleApiClient.connect();
                        }

                    });
        }
    }

    private void registerUser() {
        try {
            CreateUserTask createUserTask = new CreateUserTask(LoginActivity.this);
            createUserTask.execute(googleApiClient).get();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    try{
                        Person currentPerson = Plus.PeopleApi
                                .getCurrentPerson(googleApiClient);
                        String personName = currentPerson.getDisplayName();
                        //get user name
                        //authUser.setFullName(personName);
                        authUser.setFullName(personName);

                    }catch (Exception e){
                        

                    }finally {
                        Intent intent = new Intent(LoginActivity.this, DashboardActivity.class);
                        intent.putExtra("email", Plus.AccountApi.getAccountName(googleApiClient));
                        intent.putExtra("profile_update_required", true);
                        startActivity(intent);
                        finish();
                    }




                }
            }, SPLASH_TIME_OUT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public String splitUserName(String str) {
        String arr[] = str.split(" ");
        return arr[0];
    }

}
