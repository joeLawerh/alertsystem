package com.eliteappsolutions.jonathan.alertsystem.notifications;

/**
 * Created by jonathan on 11/4/2015.
 */
public class QuickstartPreferences {

    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";

}
