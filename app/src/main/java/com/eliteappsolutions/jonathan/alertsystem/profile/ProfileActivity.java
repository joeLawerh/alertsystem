package com.eliteappsolutions.jonathan.alertsystem.profile;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.eliteappsolutions.jonathan.alertsystem.backend.userApi.UserApi;
import com.eliteappsolutions.jonathan.alertsystem.backend.userApi.model.User;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.extensions.android.json.AndroidJsonFactory;
import com.eliteappsolutions.jonathan.alertsystem.R;
import com.eliteappsolutions.jonathan.alertsystem.auth.AuthUser;
import com.eliteappsolutions.jonathan.alertsystem.util.Config;

import java.io.IOException;

public class ProfileActivity extends ActionBarActivity {

    AuthUser authUser = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new ProfileFragment())
                    .commit();
        }

        authUser = AuthUser.getInstance(this);
        String email = getIntent().getStringExtra("email");

        if(email != null && !email.isEmpty()) {
            FetchProfileTask fetchProfileTask = new FetchProfileTask(this);
            fetchProfileTask.execute(email);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_update) {
            updateUserInfo();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
        updateUserInfo();
    }

    private void updateUserInfoOnDevice(User user) {
        authUser.setIsRegistered(true);
        authUser.setSignedIn(true);
        authUser.setUserId(user.getId());
        authUser.setEmail(user.getEmail());
        authUser.setFullName(user.getName());
        authUser.setPhoneNo(user.getPhoneNo());
        authUser.setGender(user.getGender());
        authUser.setLocation(user.getLocation());
    }

    private void updateUserInfo() {
        try {
            UpdateUserTask updateUserTask = new UpdateUserTask(ProfileActivity.this);
            updateUserTask.execute(authUser.getEmail());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class FetchProfileTask extends AsyncTask<String, Void, Void> {

        private ProgressDialog progressDialog = null;

        public FetchProfileTask(Context context) {
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage("Retrieving Profile Details...");
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(String... params) {
            try {
                User user = null;

                UserApi.Builder userApi = new UserApi.Builder(AndroidHttp.newCompatibleTransport(),
                        new AndroidJsonFactory(), null)
                        .setRootUrl(Config.APP_URL);
                UserApi service = userApi.build();

                user = service.getByEmail(params[0]).execute();

                if (user == null) {
                    // do something when user is not found
                } else {
                    updateUserInfoOnDevice(user);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressDialog.dismiss();
        }
    }
}
