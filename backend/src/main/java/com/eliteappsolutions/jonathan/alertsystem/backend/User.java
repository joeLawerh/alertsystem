package com.eliteappsolutions.jonathan.alertsystem.backend;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.condition.IfNotEmpty;

import java.io.Serializable;

/**
 * Created by jonathan on 1/27/15.
 */

@Entity
public class User implements Serializable {
    @Id
    Long id;

    @Index(IfNotEmpty.class)
    String email;

    String name;

    @Index(IfNotEmpty.class)
    String phoneNo;

    @Index(IfNotEmpty.class)
    String gender;

    @Index(IfNotEmpty.class)
    String location;

    @Index(IfNotEmpty.class)
    String donationDate;

    @Index(IfNotEmpty.class)
    Double latitude;

    @Index(IfNotEmpty.class)
    Double longitude;

    @Index(IfNotEmpty.class)
    String bloodGroup;

    @Index
    boolean isDonor;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDonationDate() { return donationDate; }

    public void setDonationDate(String donationDate) { this.donationDate = donationDate; }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public Boolean isDonor() {
        return isDonor;
    }

    public void setDonor(boolean isDonor) {
        this.isDonor = isDonor;
    }
}
